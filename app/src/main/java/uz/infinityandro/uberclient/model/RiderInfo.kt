package uz.infinityandro.uberclient.model

class RiderInfo {
    var name: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var image:String=""
}