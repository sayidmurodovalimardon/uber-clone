package uz.infinityandro.uberclient.model

class DriverInfoModel {
    var name: String? = null
    var lastName: String? = null
    var phoneNumber: String? = null
    var image:String? =null
    var rating = 0.0
}