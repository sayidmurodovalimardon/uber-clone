package uz.infinityandro.uberclient.callback

import uz.infinityandro.uberclient.model.DriverGeoModel

interface FireBaseDriverInfoListener {
    fun onDriveInfoLoadSuccess(driverGeoModel: DriverGeoModel?)
}