package uz.infinityandro.uberclient.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import okio.IOException
import uz.infinityandro.uberclient.R
import uz.infinityandro.uberclient.callback.FireBaseDriverInfoListener
import uz.infinityandro.uberclient.callback.FireBaseFailedListener
import uz.infinityandro.uberclient.databinding.FragmentHomeBinding
import uz.infinityandro.uberclient.model.DriverGeoModel
import uz.infinityandro.uberclient.model.GeoQueryModel
import uz.infinityandro.uberclient.util.Common
import java.util.*

class HomeFragment : Fragment(), OnMapReadyCallback {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var mMap: GoogleMap
    private var _binding: FragmentHomeBinding? = null
    private lateinit var mapFragment: SupportMapFragment


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    //Location
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    //Online System
    private lateinit var onlineRef: DatabaseReference
    private lateinit var currentUserRef: DatabaseReference
    private lateinit var ridersLocationRef: DatabaseReference
    private lateinit var geoFire: GeoFire

    //Load Driver
    var distance = 1.0
    val LIMIT_RANGE = 10.0
    var previousLocation: Location? = null
    var currentLocation: Location? = null

    var firstTime = true

    //listener
    lateinit var iFirebaseDriverInfoListener: FireBaseDriverInfoListener
    lateinit var iFirebaseFailedListener: FireBaseFailedListener
    var cityName = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        init()
        mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return root
    }

    @SuppressLint("MissingPermission")
    private fun init() {
        onlineRef = FirebaseDatabase.getInstance().reference.child(".info/connected")
        ridersLocationRef =
            FirebaseDatabase.getInstance().getReference(Common.RIDER_LOCATION_REFERENCE)
        currentUserRef =
            FirebaseDatabase.getInstance().getReference(Common.RIDER_LOCATION_REFERENCE)
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
        geoFire = GeoFire(ridersLocationRef)
        registerOnlineSystem()

        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.fastestInterval = 3000
        locationRequest.interval = 5000
        locationRequest.smallestDisplacement = 10f

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                val newPos = LatLng(p0.lastLocation.latitude, p0.lastLocation.longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newPos, 18f))

                if (firstTime) {
                    previousLocation = p0.lastLocation
                    currentLocation = p0.lastLocation
                    firstTime = false
                } else {
                    previousLocation = currentLocation
                    currentLocation = p0.lastLocation
                }

                if (previousLocation!!.distanceTo(currentLocation) / 1000 <= LIMIT_RANGE) {
                    loadAvailableDrivers()
                }

                geoFire.setLocation(
                    FirebaseAuth.getInstance().currentUser!!.uid,
                    GeoLocation(p0.lastLocation.latitude, p0.lastLocation.longitude)
                ) { key, error ->
                    if (error != null) {
                        Snackbar.make(
                            mapFragment.requireView(),
                            error.message,
                            Snackbar.LENGTH_LONG
                        ).show()
                    } else {
                        Snackbar.make(
                            mapFragment.requireView(),
                            "You are online",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )
        loadAvailableDrivers()


    }

    private fun loadAvailableDrivers() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Snackbar.make(requireView(), getString(R.string.permssion), Snackbar.LENGTH_SHORT)
                .show()
            return
        }
        fusedLocationProviderClient.lastLocation
            .addOnFailureListener {
                Snackbar.make(requireView(), it.message.toString(), Snackbar.LENGTH_SHORT).show()

            }.addOnSuccessListener { location ->
                val geoCoder = Geocoder(requireContext(), Locale.getDefault())
                var addressList: List<Address>?
                try {
                    addressList = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
                    cityName = addressList[0].locality
                    val driver_location_ref = FirebaseDatabase.getInstance()
                        .getReference(Common.DRIVERS_LOCATION_REFERENCE).child(cityName)
                    val gf = GeoFire(driver_location_ref)
                    val geoQuery = gf.queryAtLocation(
                        GeoLocation(location.latitude, location.longitude),
                        distance
                    )
                    geoQuery.removeAllListeners()

                    geoQuery.addGeoQueryEventListener(object : GeoQueryEventListener {
                        override fun onKeyEntered(key: String?, location: GeoLocation?) {
                            Common.drivesFound.add(DriverGeoModel(key!!, location!!))
                        }

                        override fun onKeyExited(key: String?) {

                        }

                        override fun onKeyMoved(key: String?, location: GeoLocation?) {

                        }

                        override fun onGeoQueryReady() {
                            if (distance <= LIMIT_RANGE) {
                                distance++
                                loadAvailableDrivers()
                            } else {
                                distance = 0.0
                                addDriverMarker()

                            }
                        }

                        override fun onGeoQueryError(error: DatabaseError?) {
                            Snackbar.make(mapFragment.requireView(), error!!.message, Snackbar.LENGTH_SHORT).show()

                        }

                    })
                    driver_location_ref.addChildEventListener(object :ChildEventListener{
                        override fun onChildAdded(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {
                            val geoQueryModel=snapshot.getValue(GeoQueryModel::class.java)
                            val geoLocation=GeoLocation(geoQueryModel!!.l!![0],geoQueryModel!!.l!![1])
                            val driverGeoModel=DriverGeoModel(snapshot.key,geoLocation)
                            val newDriverLocation=Location("")
                            newDriverLocation.latitude=geoLocation.latitude
                            newDriverLocation.longitude=geoLocation.longitude
                            val newDistane=location.distanceTo(newDriverLocation)/1000
                            if (newDistane<=LIMIT_RANGE){
                                findDriverByKey(driverGeoModel)
                            }
                        }

                        override fun onChildChanged(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {

                        }

                        override fun onChildRemoved(snapshot: DataSnapshot) {

                        }

                        override fun onChildMoved(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {

                        }

                        override fun onCancelled(error: DatabaseError) {
                            Snackbar.make(mapFragment.requireView(), error.message, Snackbar.LENGTH_SHORT).show()

                        }

                    })
                } catch (e: IOException) {

                }
            }
    }

    private fun addDriverMarker() {

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        Dexter.withContext(requireContext())
            .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    mMap.isMyLocationEnabled = true
                    mMap.uiSettings.isMyLocationButtonEnabled = true
                    mMap.setOnMyLocationClickListener {
                        fusedLocationProviderClient.lastLocation
                            .addOnFailureListener { e ->
                                Toast.makeText(
                                    requireContext(), e.message, Toast.LENGTH_SHORT
                                ).show()
                            }.addOnSuccessListener { succes ->
                                val userLan = LatLng(succes.latitude, succes.longitude)
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLan, 18f))
                            }
                        true
                    }
                    val locationButton = (mapFragment.requireView()
                        .findViewById<View>("1".toInt())
                        .parent as View).findViewById<View>("2".toInt())
                    val params = locationButton.layoutParams as RelativeLayout.LayoutParams
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                    params.bottomMargin = 50


                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    Toast.makeText(
                        requireContext(),
                        "Permission:" + p0!!.permissionName + "was denied",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {

                }

            }).check()

        googleMap.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                requireContext(), R.raw.uber_maps_style
            )
        )


    }

    override fun onDestroy() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        geoFire.removeLocation(FirebaseAuth.getInstance().currentUser!!.uid)
        onlineRef.removeEventListener(onlineValueEventListener)
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private val onlineValueEventListener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            if (snapshot.exists()) {
                currentUserRef.onDisconnect().removeValue()
            }

        }

        override fun onCancelled(error: DatabaseError) {
            Snackbar.make(mapFragment.requireView(), error.message, Snackbar.LENGTH_SHORT).show()

        }
    }

    override fun onResume() {
        super.onResume()
        registerOnlineSystem()
    }

    private fun registerOnlineSystem() {
        onlineRef.addValueEventListener(onlineValueEventListener)
    }
}