package uz.infinityandro.uberclient.util

import uz.infinityandro.uberclient.model.DriverGeoModel
import uz.infinityandro.uberclient.model.RiderInfo

object Common {
    fun buildWelcomeMessage(): String {
        return StringBuilder("Welcome, ")
            .append(currentRider!!.name)
            .append(" ")
            .append(currentRider!!.lastName)
            .toString()

    }


    val drivesFound: HashSet<DriverGeoModel>()
    var currentRider:RiderInfo?=null

    const val DRIVERS_LOCATION_REFERENCE = "DriversLocation"
    const val RIDER_INFO_REFERENCE="Riders"
    const val RIDER_LOCATION_REFERENCE="RidersLocation"
}